package com;

public class Student extends Man {

    protected int yearsOfStudy;

    public Student(String name, String gender, int age, int weight, int yearsOfStudy){
        super(name, gender, age,weight);

    }

    public int getYearsOfStudy() {
        return yearsOfStudy;
    }

    public void setYearsOfStudy(int yearsOfStudy) {
        this.yearsOfStudy = yearsOfStudy;
    }

    public void increaseYearsOfStudy(int years) {
        this.yearsOfStudy += years;
    }
}
