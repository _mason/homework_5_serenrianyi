package mat;

import java.util.Random;

public class Matrix {

    private static Random RANDOM = new Random();
    private int height;
    private int width;
    private int[][] matrix;

    public Matrix(int height, int width) {
        this.height = height;
        this.width = width;
        this.matrix = new int[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                this.matrix[i][j] = RANDOM.nextInt(9);
            }
        }
    }

    public Matrix(int[][] matrix) {
        this.height = getHeight();
        this.width = getWidth();
        this.matrix = new int[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                this.matrix[i][j] = matrix[i][j];
            }
        }
        this.matrix = matrix;
    }

    public void setMatrix(int[][] matrix) {
        this.height = getHeight();
        this.width = getWidth();
        this.matrix = matrix;
    }

    public int[][] matrixSum(int[][] arr) {
        int[][] newMatrix = matrix;
        if ((matrix.length != arr.length) || (matrix[0].length != arr[0].length)) {
            System.out.println("Не равные матрицы");
        } else {
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[0].length; j++) {
                    newMatrix[i][j] = matrix[i][j] + arr[i][j];
                }
            }
        }
        return newMatrix;
    }

    public int[][] matrixMultiplication(int x) {
        int[][] newMatrix = matrix;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                newMatrix[i][j] = matrix[i][j] * x;
            }
        }
        return newMatrix;
    }

    public int[][] matrixTranspose() {
        int[][] newMatrix = matrix;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                newMatrix[i][j] = matrix[j][i];
            }
        }
        return newMatrix;
    }

    public int[][] matrixMultiplicationOfTwo( int[][] arr) {
        int[][] newMatrix = matrix;
        if ((matrix.length != matrix[0].length) || (arr.length != arr[0].length) || (matrix.length != arr.length)) {
            System.out.println("Не равные матрицы");
        } else {
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix.length; j++) {
                    for (int k = 0; k < matrix.length; k++) {
                        newMatrix[i][j] += matrix[i][k] * arr[k][j];
                    }
                }
            }
        }
        return newMatrix;
    }

    public void matrixPrint() {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int[][] getMatrix() {
        return matrix;
    }
}
